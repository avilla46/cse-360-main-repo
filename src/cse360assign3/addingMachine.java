/**	@author Austin Villanueva
 *  @version 1.0
 *  @since 2019-10-20
 *	<h4>Class: CSE 360 Introduction to Software Engineering (2019 Fall)</h4>
 *	<h5>Class ID: 2019Fall-X-CSE301-79346</h5>
 *	<h1>Assignment #3</h1>
 *	Description: The following assignment is a class that adds or subtracts
 *	numbers to a total, and can print out the total and history of additions/subtractions 
 *  <p>
 */

// https://avilla46@bitbucket.org/avilla46/cse-360-main-repo.git

package cse360assign3;
/**	We import ArrayLists as they are far more powerful and dynamic than the default java arrays*/
import java.util.ArrayList;

public class addingMachine {
	
	/** Initializing protected class variables*/
	protected int total;
	protected ArrayList<String> numHistory = new ArrayList<String>();
	
	/** The constructor assigning values to the class variables*/
	public addingMachine () {
		total = 0;  // not needed - included for clarity
		numHistory.add("0");
	}
	
	/** getTotal(): Returns the current total value
	 * @return total
	 */
	public int getTotal () {
		return total;
	}
	
	/** add(): Adds the value to the total
	 * @param value
	 */
	public void add (int value) {
		total += value;
		numHistory.add(" + " + value);
	}
	
	/** subtract(): Subtracts the value from the total
	 * @param value
	 */
	public void subtract (int value) {
		total -= value;
		numHistory.add(" - " + value);
	}
	
	/** toString(): Loops through the ArrayList and adds it to an easy to understand string */
	public String toString () {
		String historyString = "";
		for(int index = 0; index < numHistory.size(); index++){
			historyString = historyString + numHistory.get(index);
		}
		return historyString;
	}
	
	/** clear(): Though there were no instructions to do anything to this method, I made it clear
	 *  the history of the ArrayList
	 */
	public void clear() {
		numHistory.clear();
		numHistory.add(total + ""); //The + "" is there so it thinks of 'total' as a string
	}
}