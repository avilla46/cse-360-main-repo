package cse360assign3;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class calculatorTest {

	@Test
	void test() {
		calculator calc = new calculator();
		calc.add(20);
		calc.add(50);
		String hist = calc.toString();
		System.out.println(hist);
		calc.clear();
		hist = calc.toString();
		System.out.println(hist);
		calc.subtract(60);
		System.out.println(calc.getTotal());
		calc.power(5);
		System.out.println(calc.getTotal());
		calc.mult(3);
		System.out.println(calc.getTotal());
		calc.div(3);
		System.out.println(calc.getTotal());
		calc.power(0);
		System.out.println(calc.getTotal());
		calc.add(27);
		calc.power(-23);
		System.out.println(calc.getTotal());
		hist = calc.toString();
		System.out.println(hist);
		calc.clear();
		hist = calc.toString();
		System.out.println(hist);
		assertTrue(calc.getTotal()==0, "The total is not zero!");
		
	}

}
