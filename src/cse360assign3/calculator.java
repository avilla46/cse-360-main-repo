/**	@author Austin Villanueva
 *  @version 1.0
 *  @since 2019-10-20
 *	<h4>Class: CSE 360 Introduction to Software Engineering (2019 Fall)</h4>
 *	<h5>Class ID: 2019Fall-X-CSE301-79346</h5>
 *	<h1>Assignment #3</h1>
 *	Description: The following assignment is a class that extends the addingMachine class
 *	and adds multiplication, division, and power functions.
 *  <p>
 */

// https://avilla46@bitbucket.org/avilla46/cse-360-main-repo.git

package cse360assign3;

public class calculator extends addingMachine{
	
	/** The constructor assigning values to the class variables*/
	public calculator () {
		
	}
	
	/** mult(): Multiplies the value to the total
	 * @param value
	 */
	public void mult (int value) {
		super.total *= value;
		numHistory.add(" * " + value);
	}
	
	/** div(): Divides the value from the total
	 * @param value
	 */
	public void div(int value) {
		if(value == 0) {
			super.total = 0;
		}else {
			super.total /= value;
		}
		numHistory.add(" / " + value);
	}
	
	/** power(): Gets the power of the total
	 * @param value
	 */
	public void power(int value) {
		int originalTotal = super.total;
		if(value < 0) {
			super.total = 0;
		}else if(value == 0) {
			super.total = 1;
		}else {
			for(int i = value; i > 1; i--) {
				super.total *= originalTotal;
			}
		}
		numHistory.add(" ^ " + value);
	}
}